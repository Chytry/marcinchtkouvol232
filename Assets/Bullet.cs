﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    GameObject tower;
    Vector3 starting_pos;
    float destroy_dist;
    void Start()
    {
        destroy_dist = Random.Range(1f, 4f);
        tower = GameObject.Find("tower");
        starting_pos = transform.position;
        GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-20, 20), Random.Range(-20, 20)), ForceMode2D.Impulse);
    }

    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, starting_pos) > destroy_dist)
        {
            Instantiate(tower, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}
