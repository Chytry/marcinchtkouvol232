﻿using UnityEngine;

public class tower : MonoBehaviour
{
    public GameObject bullet;
    float delta_time = 0f;
    bool time_passed = false;
    int shots_number = 0;

    private void Start()
    {
        GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
    }
    void FixedUpdate()
    {
        delta_time += Time.deltaTime;
        
        if(delta_time >= 6f && time_passed == false) time_passed = true;

        if(shots_number >= 12) time_passed = false;

        if (delta_time >= 0.5f && time_passed)
        {
            GetComponent<SpriteRenderer>().color = new Color (255, 0, 0);
            transform.Rotate(0f, 0f, Random.Range(15, 45));
            ShootBullet();
            delta_time = 0f;
        }

        if(time_passed == false)
        {
            GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
        }
    }

    void ShootBullet()
    {
        Quaternion gun_rotation = transform.rotation;
        Instantiate(bullet, transform.position, gun_rotation);
        shots_number += 1;
    }
}
